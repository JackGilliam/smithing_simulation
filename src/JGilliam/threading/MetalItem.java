package JGilliam.threading;

import java.util.Random;

public class MetalItem{

	private int completion;
	private int heat;
	private int mistakes;
	private static final int MAX = 100;
	private static final int MIN = 0;
	private static Random randNum = new Random(); 
	private static final int COMPLETIONRANGE = 5;
	private static final int COOLDOWNBYAIR = 2;
	private static final int MISTAKERANGE = 10;
	public MetalItem()
	{
		completion = 0;
		heat = 0;
		mistakes = 0;
	}
	public void completeFurther()
	{
		int isMistake = randNum.nextInt(MISTAKERANGE);
		if(isMistake == 5)
		{
			System.out.println("You made a mistake");
			makeAMistake();
		}
		else
		{
			int amountCompleted = randNum.nextInt(COMPLETIONRANGE)+3;
			if(completion + amountCompleted > MAX)
			{
				amountCompleted = MAX - completion;
			}
			completion += amountCompleted;
		}
	}
	public void increaseTemperature(Forge forge)
	{
		int heatAdd = forge.getTemperature()/30;
		if(heat + heatAdd > MAX)
		{
			heatAdd = MAX - heat;
		}
		heat += heatAdd;
	}
	public void decreaseTemperature()
	{
		int cooldownAmount = COOLDOWNBYAIR;
		if(heat - cooldownAmount < MIN)
		{
			cooldownAmount = Math.abs(MIN - heat);
		}
		heat -= cooldownAmount;
	}
	public void makeAMistake()
	{
		mistakes++;
	}
	public int getCompletion()
	{
		return completion;
	}
	public int getTemperature()
	{
		return heat;
	}
	public int getMistakes()
	{
		return mistakes;
	}
}
