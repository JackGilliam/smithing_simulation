package JGilliam.threading;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Anvil extends JButton{

	private static final long serialVersionUID = 1L;
	private boolean active;
	public Anvil(Image image)
	{
		ImageIcon picture = new ImageIcon(image);
		this.setIcon(picture);
		active = false;
	}
	public void switchActive()
	{
		active = !active;
	}
	public boolean isActive()
	{
		return active;
	}
	public void coolDownMetal(MetalItem item)
	{
		item.decreaseTemperature();
	}
	public void increaseProgress(MetalItem item)
	{
		item.completeFurther();
	}
}
