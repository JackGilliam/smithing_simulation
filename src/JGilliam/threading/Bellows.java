package JGilliam.threading;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Bellows extends JButton{

	private static final long serialVersionUID = 1L;
	public Bellows(Image image)
	{
		ImageIcon picture = new ImageIcon(image);
		this.setIcon(picture);
	}
	public static void blowAir(Forge forge)
	{
		forge.heatUp();
	}
}
