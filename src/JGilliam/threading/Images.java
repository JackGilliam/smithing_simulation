package JGilliam.threading;
import java.awt.*;
import javax.swing.*;

/*
 * 
 */
public class Images extends JComponent
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5593721533512716823L;
	private Image content;
    public Images(int x, int y, int w, int h, String s)  {
        super();
        setBounds(x, y, w, h);
        java.net.URL imgURL = getClass().getResource(s);
        if (imgURL != null) {
            content = new ImageIcon(imgURL).getImage();
        } else {
            System.err.println("Couldn't find file: " + s);
        }
    }
  
    public void paint(Graphics g)  {
        g.drawImage(content, 0, 0, getWidth(), getHeight(), this);
        paintChildren(g);
    }
}
