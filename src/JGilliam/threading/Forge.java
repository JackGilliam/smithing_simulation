package JGilliam.threading;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Forge extends JButton{

	private boolean active;
	private static final long serialVersionUID = 1L;
	private static int heat = 0;
	private static final int HEATCHANGE = 20;
	private static final int MAX = 100;
	private static final int MIN = 0;
	public Forge(Image image)
	{
		ImageIcon picture = new ImageIcon(image);
		this.setIcon(picture);
		active = false;
		heat = 0;
	}
	public void switchActive()
	{
		active = !active;
	}
	public boolean isActive()
	{
		return active;
	}
	public int getTemperature()
	{
		return heat;
	}
	public void heatItem(MetalItem anItem)
	{
		anItem.increaseTemperature(this);
	}
	public void heatUp()
	{
		int heatAmount = HEATCHANGE/2;
		if(heat + heatAmount > MAX)
		{
			heatAmount = MAX - heat;
		}
		heat += heatAmount;
	}
	public void coolDown()
	{
		int coolAmount = HEATCHANGE/5;
		if(heat - coolAmount < MIN)
		{
			coolAmount = Math.abs(MIN - heat);
		}
		heat -= coolAmount;
	}
}
