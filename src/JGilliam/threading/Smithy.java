package JGilliam.threading;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class Smithy extends JFrame {
	/*
	 * Author: Jack Gilliam
	 * Date: 5/7/2011
	 * TimeSpent: 2.3 hours
	 */
	private static final long serialVersionUID = 1L;
	private JProgressBar heat, mistakes, completion, itemHeat;
	private Forge forge;
	private Anvil anvil;
	private Bellows bellows;
	private MetalItem item;
	private JButton start;
	private int count;
	private int numItemsForged = 0;
	private JLabel heatLabel, mistakesLabel, completionLabel, itemHeatLabel;
	private static final int CANSTARTANVIL = 50;
	private static final int CANSTARTFORGE = 50;
	public Smithy()
	{
		this.setLayout(new BorderLayout());
		start = new JButton("Start creating an Item");
		URL imgURL = getClass().getResource("Bellows.jpg");
		Image bellowsImg = Toolkit.getDefaultToolkit().getImage(imgURL);
		bellows = new Bellows(bellowsImg);
		URL imgURL2 = getClass().getResource("Forge.jpg");
		Image forgeImg = Toolkit.getDefaultToolkit().getImage(imgURL2);
		forge = new Forge(forgeImg);
		URL imgURL3 = getClass().getResource("Anvil.jpg");
		Image anvilImg = Toolkit.getDefaultToolkit().getImage(imgURL3);
		anvil = new Anvil(anvilImg);
		
		
		JPanel NorthPanel = new JPanel(new BorderLayout());
		JPanel CenterPanel = new JPanel(new BorderLayout());
		JPanel SouthPanel = new JPanel(new BorderLayout());
		JPanel buttonLabels = new JPanel(new GridLayout());
		JPanel barLabels = new JPanel(new GridLayout());
		JPanel buttons = new JPanel(new GridLayout());
		JPanel bars = new JPanel(new GridLayout());
		JPanel itemInfo = new JPanel(new BorderLayout());
		
		heatLabel = new JLabel("Heat - %0");
		mistakesLabel = new JLabel("Mistakes - 0");
		completionLabel = new JLabel("Progress - %0");
		itemHeatLabel = new JLabel("Item Heat - %0");
		
		JLabel bellowsLabel = new JLabel("Bellows");
		JLabel forgeLabel = new JLabel("Forge");
		JLabel anvilLabel = new JLabel("Anvil");
		
		heat = new JProgressBar(0, 100);
		mistakes = new JProgressBar(0, 10);
		completion = new JProgressBar(0, 100);
		itemHeat = new JProgressBar(0, 100);
		
		
		buttons.add(bellows);
		buttons.add(forge);
		buttons.add(anvil);
		
		bellows.setEnabled(false);
		forge.setEnabled(false);
		anvil.setEnabled(false);
		
		addListener(bellows, forge, anvil, start);
		
		
		buttonLabels.add(bellowsLabel);
		buttonLabels.add(forgeLabel);
		buttonLabels.add(anvilLabel);
		bars.add(heat);
		bars.add(mistakes);
		bars.add(completion);
		barLabels.add(heatLabel);
		barLabels.add(mistakesLabel);
		barLabels.add(completionLabel);
		itemInfo.add(itemHeatLabel, BorderLayout.NORTH);
		itemInfo.add(itemHeat, BorderLayout.SOUTH);
		CenterPanel.add(start, BorderLayout.SOUTH);
		CenterPanel.add(itemInfo, BorderLayout.NORTH);
		NorthPanel.add(barLabels, BorderLayout.NORTH);
		NorthPanel.add(bars, BorderLayout.SOUTH);
		SouthPanel.add(buttonLabels, BorderLayout.NORTH);
		SouthPanel.add(buttons, BorderLayout.SOUTH);
		add(NorthPanel, BorderLayout.NORTH);
		add(SouthPanel, BorderLayout.SOUTH);
		add(CenterPanel, BorderLayout.CENTER);
		pack();
	}
	private void addListener(JButton... buttons)
	{
		for(JButton button: buttons)
		{
			button.addActionListener(new ActionListener()
			{
				public void actionPerformed(final ActionEvent e)
				{
					if(e.getSource() == start)
					{
						Thread forging = new Thread(new Runnable() {
							public void run()
							{
								((JButton) e.getSource()).setEnabled(false);
								bellows.setEnabled(true);
								item = new MetalItem();
								System.out.println("Forging");
								count = 0;
								forgeAnItem();
							}
						});
						forging.start();
					}
					if(e.getSource() == bellows)
					{
						if(forge.getTemperature() != 100)
						{
							System.out.println("Forge is heated up");
							forge.heatUp();
							updateProgressBars();
						}
					}
					else if(e.getSource() == forge)
					{
						Thread forgeThread = new Thread(new Runnable(){
							
							public void run()
							{
								forge.switchActive();
								if(anvil.isActive())
								{
									anvil.switchActive();
								}
								forgeActivate();
							}
						});
						forgeThread.start();
					}
					else if(e.getSource() == anvil)
					{
						Thread anvilThread = new Thread(new Runnable(){
							public void run()
							{
								anvil.switchActive();
								if(forge.isActive())
								{
									forge.switchActive();
								}
								anvilActivate();
							}});
						anvilThread.start();
					}
			}
			});
		}
	}
	private synchronized void anvilActivate()
	{
			if(item != null)
			{
				if(anvil.isActive())
				{
					SwingUtilities.invokeLater(new Runnable(){
						public void run()
						{
							if(item.getTemperature() > CANSTARTANVIL)
							{
								System.out.println("Pounding the Metal on the anvil");
								
								anvil.increaseProgress(item);
							}
							else
							{
								System.out.println("Metal is too cold");
								anvil.switchActive();
							}
							anvil.coolDownMetal(item);
							anvilActivate();

						}
					});
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
					}				}
				else
				{
					System.out.println("Stopped pounding Metal on the Anvil");

				}
			}
			updateProgressBars();
		}
	private synchronized void forgeActivate()
	{
			if(item != null)
			{
				if(forge.isActive())
				{
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
					}
					SwingUtilities.invokeLater(new Runnable() {
						
						public void run()
						{
							System.out.println("Heating Item");
							if(forge.getTemperature() > CANSTARTFORGE)
							{
								forge.heatItem(item);
							}
							else
							{
								System.out.println("Forge is too cold");
								forge.switchActive();
							}
							forgeActivate();
						}
					});
					
				}
				else
				{
					System.out.println("Taken out of Forge");
				}
			}
			updateProgressBars();
		}
	public static void main(String[] args) {
		Smithy smith = new Smithy();
		smith.setDefaultCloseOperation(EXIT_ON_CLOSE);
		smith.setVisible(true);
	}
	public void updateProgressBars()
	{
		if(item != null)
		{
			heat.setValue(forge.getTemperature());
			mistakes.setValue(item.getMistakes());
			completion.setValue(item.getCompletion());
			itemHeat.setValue(item.getTemperature());
		}
		else
		{
			heat.setValue(0);
			mistakes.setValue(0);
			completion.setValue(0);
			itemHeat.setValue(0);
		}

		heatLabel.setText("Heat - %"+ heat.getValue());
		mistakesLabel.setText("Mistakes - " + mistakes.getValue());
		completionLabel.setText("Progress - %" + completion.getValue());
		itemHeatLabel.setText("Item Heat - %" + itemHeat.getValue());
	}
	public synchronized void forgeAnItem()
	{
		if(item != null)
		{
			if(item.getCompletion() < 100)
			{
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
				}
			
				SwingUtilities.invokeLater(new Runnable() {
				public void run()
				{
						if(forge.getTemperature() > CANSTARTFORGE)
						{
							forge.setEnabled(true);
						}
						else
						{
							forge.setEnabled(false);
						}
						if(item.getTemperature() > CANSTARTANVIL)
						{
							anvil.setEnabled(true);
						}
						else 
						{
							anvil.setEnabled(false);
						}
						if(item.getMistakes() == 10)
						{
							item = null;
							System.out.println("The metal you were working on broke");
						}
						if(count%3 == 0)
						{
							forge.coolDown();
						}
						if(!forge.isActive() && !anvil.isActive() && count%4 == 0)
						{
							item.decreaseTemperature();
						}
						count++;
						forgeAnItem();
				}
				});
				updateProgressBars();
			}
			else
			{
				numItemsForged++;
				System.out.println("Item has been forged");
				System.out.printf("Mistakes you made %d\n", item.getMistakes());
				start.setText("Number of Items Forged: " + numItemsForged + " - Press to forge Another");
				start.setEnabled(true);
				anvil.setEnabled(false);
				bellows.setEnabled(false);
				forge.setEnabled(false);
				updateProgressBars();
			}
		}
		else
		{
			updateProgressBars();
			start.setEnabled(true);
			anvil.setEnabled(false);
			bellows.setEnabled(false);
			forge.setEnabled(false);
			System.out.println("Item is broken :P");
		}
		}
	}